﻿using System;

namespace Task2_ConsoleInput
{
    class Program
    {
        static void Main(string[] args)
        {
            string yourName = "";
            Console.Write("What is your name: ");
            while(yourName.Replace(" ", string.Empty) == "")
            {
                yourName = Console.ReadLine();
            }

            // Removes space since I don't see it as a character.
            Console.WriteLine($"Hello {yourName}, your name is {yourName.Replace(" ", string.Empty).Length} characters long" +
                $" and starts with a {yourName.Replace(" ", string.Empty)[0]}");
        }
    }
}
