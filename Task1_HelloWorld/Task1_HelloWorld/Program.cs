﻿using System;

namespace Task1_HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            // Some simple variable names since it is a short assignment
            string[] teachers = new string[]
            {
                "Dean",
                "Greg"
            };
            string me = "Jon Erik";
            HelloTeachers(teachers, me);
        }

        static void HelloTeachers(string[] teachers, string me)
        {
            Console.WriteLine($"Greetings {teachers[0]} and {teachers[1]}! My name is {me}" +
                $" and I look forward to the next three months.");
        }
    }
}
